﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TGI_vize_odevi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void acToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dosya = new OpenFileDialog();
            dosya.Filter = "Resim dosyası |.jpg;.nef;.png| video|.avi| Tüm Dosyalar |.";
            dosya.Title = "";
            dosya.ShowDialog();
            string DosyaYolu = dosya.FileName;
            pictureBox1.ImageLocation = DosyaYolu;
        }

        private void kaydetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "JPEG files(.jpeg)|.jpeg";
            if (DialogResult.OK == sfd.ShowDialog())
            {
                this.pictureBox2.Image.Save(sfd.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
        }

        private void cıkısToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private Bitmap averageOrtalamaYöntemi(Bitmap cikisresmi)
        {
            for (int i = 0; i < cikisresmi.Height - 1; i++)
            {
                for (int j = 0; j < cikisresmi.Width - 1; j++)
                {
                    int deger = (cikisresmi.GetPixel(j, i).R + cikisresmi.GetPixel(j, i).G + cikisresmi.GetPixel(j, i).B * 0) / 3;
                    Color renk;
                    renk = Color.FromArgb(deger, deger, deger);
                    cikisresmi.SetPixel(j, i, renk);
                }
            }
            return cikisresmi;

        }
        private void averageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(pictureBox1.Image);
            Bitmap gri = averageOrtalamaYöntemi(image);
            pictureBox2.Image = gri;
        }


        private Bitmap bT709(Bitmap cikisresmi)

        {

            Color OkunanRenk, DonusenRenk;

            Bitmap GirisResmi = new Bitmap(pictureBox1.Image);

            int ResimGenisligi = GirisResmi.Width;
            int ResimYüksekligi = GirisResmi.Height;

            Bitmap CikisResmi = new Bitmap(ResimGenisligi, ResimYüksekligi);
            //çıkış resmini oluşturuyor.boyutları giriş resmi ile aynı olur

            int GriDeger = 0;
            for (int x = 0; x < ResimGenisligi; x++)
            {
                for (int y = 0; y < ResimYüksekligi; y++)
                {
                    OkunanRenk = GirisResmi.GetPixel(x, y);

                    double R = OkunanRenk.R;
                    double G = OkunanRenk.G;
                    double B = OkunanRenk.B;

                    //GriDeger = Convert.ToInt16((R+G+B)/3);

                    GriDeger = Convert.ToInt16(R * 0.2125 + G * 0.7154 + B * 0.072);

                    DonusenRenk = Color.FromArgb(GriDeger, GriDeger, GriDeger);
                    //Gri renk değeri bulunduktan sonra, R,G,B renk değerlerinin hepsi aynı renk koduna sahip olmalıdır.
                    //R=GriDegeri, G=GriDegeri, B=GriDegeri 
                    CikisResmi.SetPixel(x, y, DonusenRenk);
                }
            }
            return CikisResmi;
        }




        private void bT709ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(pictureBox1.Image);
            Bitmap gri = bT709(image);
            pictureBox2.Image = gri;


        }


        //Açıklık(Desaturation) yöntemi ile griye çevirme

        private Bitmap açıklıkDesaturationYöntemi(Bitmap cikisresmi)
        {

            for (int i = 0; i < cikisresmi.Height - 1; i++)
            {
                for (int j = 0; j < cikisresmi.Width - 1; j++)
                {
                    int deger = (cikisresmi.GetPixel(j, i).R + cikisresmi.GetPixel(j, i).G + cikisresmi.GetPixel(j, i).B) / 3;
                    Color renk;
                    renk = Color.FromArgb(deger, deger, deger);
                    cikisresmi.SetPixel(j, i, renk);
                }
            }
            return cikisresmi;

        }
        private void desaturationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(pictureBox1.Image);
            Bitmap gri = açıklıkDesaturationYöntemi(image);
            pictureBox2.Image = gri;
        }
    }
}
